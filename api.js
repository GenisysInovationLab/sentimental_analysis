/**
 * Text Analysis Api hit to paralleldot.com
 * @author - Abhishek Dixit < abhishek.dixit@genisys-group.com >
 */
"use strict"
var qs = require("querystring");
var http = require("http");
const request = require('request');

var methods = {
    //Sentimental Analysis API Hit
    textAnalysis : function(apiString, endpoint, callback){
        request.post({            
            "url": "https://apis.paralleldots.com/v3" + endpoint,            
            "body": apiString,            
            "headers": {
                'content-type' : 'application/x-www-form-urlencoded'
            }
        },function(error, response, body){
            if(error)
                console.log(error);
            else{
                callback(body);
            }
        })        
    }
};

module.exports = methods;