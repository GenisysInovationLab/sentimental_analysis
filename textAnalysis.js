"use strict"
/**
 * Text Analysis Bot Listner File
 * @author - Abhishek Dixit < abhishek.dixit@genisys-group.com >
 */

const api          = require('./api.js');
const key           = "YSLYnOj7D0DCaQDNU1PWsSOZanSGaOpCyCsjXurJ6x4";

var endpoints = {
    "Sentiment"  : "/sentiment",
    "Similarity" : "/similarity",
    "Entity"     : "/ner",
    "KeyWord"    : "/keywords",
    "Taxonomy"   : "/taxonomy",
    "Emotions"   : "/emotion",
    "Intent"     : "/intent",
    "Abuse"      : "/abuse",
    "Text_Parser": "/text_parser",
    "Language"   : "/language_detection"
}

var methods = {
    //Sentiment Analysis
    sentimentAnalysis : function(text, callback){
        var apiString = "text="+encodeURIComponent(text) + "&lang_code=en&api_key=" + key;        
        api.textAnalysis(apiString, endpoints.Sentiment, function(res){
            callback(res);
        });
    },

    //Similarity Analysis
    similarityAnalysis : function(text1, text2, callback){
        var apiString = "text_1="+encodeURIComponent(text1) +"&text_2="+encodeURIComponent(text2) +"&api_key=" + key;        
        api.textAnalysis(apiString, endpoints.Similarity, function(res){
            callback(res);
        });
    },

    //Entity Analysis
    entityAnalysis : function(text, callback){
        var apiString = "text="+encodeURIComponent(text) +"&api_key=" + key;        
        api.textAnalysis(apiString, endpoints.Entity, function(res){
            callback(res);
        });
    },

    //KeyWord Analysis
    keywordAnalysis : function(text, callback){
        var apiString = "text="+encodeURIComponent(text) +"&api_key=" + key;        
        api.textAnalysis(apiString, endpoints.KeyWord, function(res){
            callback(res);
        });
    },

    //Taxonomy Analysis
    taxonomyAnalysis : function(text, callback){
        var apiString = "text="+encodeURIComponent(text) +"&api_key=" + key;        
        api.textAnalysis(apiString, endpoints.Taxonomy, function(res){
            callback(res);
        });
    },

    //Emotions Analysis
    emotionAnalysis : function(text, callback){
        var apiString = "text="+encodeURIComponent(text) +"&lang_code=en&api_key=" + key;        
        api.textAnalysis(apiString, endpoints.Emotions, function(res){
            callback(res);
        });
    },

    //Intent Analysis
    intentAnalysis : function(text, callback){
        var apiString = "text="+encodeURIComponent(text) +"&api_key=" + key;        
        api.textAnalysis(apiString, endpoints.Intent, function(res){
            callback(res);
        });
    },

    //Abuse Analysis
    abuseAnalysis : function(text, callback){
        var apiString = "text="+encodeURIComponent(text) +"&api_key=" + key;        
        api.textAnalysis(apiString, endpoints.Abuse, function(res){
            callback(res);
        });
    },

    //Text parser Analysis
    textParserAnalysis : function(text, callback){
        var apiString = "text="+encodeURIComponent(text) +"&api_key=" + key;        
        api.textAnalysis(apiString, endpoints.Text_Parser, function(res){
            callback(res);
        });
    },

    //Language Analysis
    languageAnalysis : function(text, callback){
        var apiString = "text="+encodeURIComponent(text) +"&api_key=" + key;        
        api.textAnalysis(apiString, endpoints.Language, function(res){
            callback(res);
        });
    },
}

module.exports = methods;

//Unit Testing
// methods.languageAnalysis("India lost today in cricket match", function(res){
//     console.log(res);
// });

