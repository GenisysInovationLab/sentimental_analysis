const TelegramBot = require('node-telegram-bot-api');

// replace the value below with the Telegram token you receive from @BotFather
const token = '617900983:AAE9_P8PazdUhSn5zUjDT77zl0l6BX_HVRE';

// Create a bot that uses 'polling' to fetch new updates
const bot = new TelegramBot(token, {
    polling: true
});
const text = require('./textAnalysis.js');


// Listen for any kind of message. There are different kinds of
// messages.
bot.on('message', (msg) => {
    
    const chatId = msg.chat.id;
    setTimeout(function () {
        bot.sendMessage(chatId, "Processing your text, Please wait...");
    }, 100);

    //Language Analysis
    text.languageAnalysis(msg.text, function (res) {
        res = JSON.parse(res);
        var message = "Language for this text is " + JSON.stringify(res.output) +
            " with confidence " + (res.prob * 100).toPrecision(2) + "%";

        bot.sendMessage(chatId, message);
    });

    //Sentiment Analysis
    text.sentimentAnalysis(msg.text, function(res) {
        res = JSON.parse(res);
        console.log(res);
        console.log(res.probabilities[res.sentiment]);
        var message = "Sentiment Analysis Report:\n\n"+
                        "It is a " + res.sentiment +" statement with " + (res.probabilities[res.sentiment]*100).toPrecision(2) + "% confidence";
        bot.sendMessage(chatId, message);
    });
    

    //Emotions Analysis
    text.emotionAnalysis(msg.text, function(res) {
        res = JSON.parse(res);
        
        var message = "Text Emotion Report:\n\n"
                        +"Angry: \t"+     (res.emotion.probabilities.Angry).toPrecision(2)
                        +"\nHappy: \t"+   (res.emotion.probabilities.Happy).toPrecision(2)
                        +"\nBored: \t"+   (res.emotion.probabilities.Bored).toPrecision(2)
                        +"\nSad: \t"+     (res.emotion.probabilities.Sad).toPrecision(2)
                        +"\nSarcasm: \t"+ (res.emotion.probabilities.Sarcasm).toPrecision(2)
                        +"\nExcited: \t"+ (res.emotion.probabilities.Excited).toPrecision(2)
                        +"\nFear: \t"+    (res.emotion.probabilities.Fear).toPrecision(2)
                        +"\n\nAnd predicted emotion is "+res.emotion.emotion;
        bot.sendMessage(chatId, message);
        
    });
});